/* eslint-disable no-console */
var util = require( 'util' );
var allowProcess = require( './index.js' );

function log( ) {

	var arr = Array.prototype.slice.call( arguments );

	arr.unshift( process.pid );
	util.log.apply( util, arr );

}

log( 'starting' );

allowProcess.allow( {
	key: 'test',
	maxParallel: 2,
	db: {
		connection: 'mongodb://127.0.0.1/',
		database: 'mydatabase',
		collection: 'lock'
	},
	ifAllowed: dbConnected,
	ifDisallowed: disallowed
} );

function dbConnected( _db ) {
	log( 'connected - working for ~ 5000 milliseconds' );
	setTimeout( doneTest, 5000 );
}

function disallowed( /*dbShutdown*/ err ) {
	console.log( 'disallowed', err || 'OK' );
	shutdown();
}

function doneTest(){
	log( 'shutting down' );
	// db.shutdown( shutdown );
	allowProcess.cleanUp( shutdown );
}

function shutdown(){
	log( 'shut down' );
}
