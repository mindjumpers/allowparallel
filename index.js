var DEFAULT_MAX_PARALLEL = 5;
var OFFSET_ONE = 1;

// allow tries to add this task to a list of running tasks
// kept in the lockCollection under key 'key'.
// if there are already `maxParallel` tasks, runs
// `ifDisallowed`, otherwise `ifAllowed` (and does not add this task)


var mongodb = require( 'mongodb' );

var didAllow = false;
var lockCollection;
var allowedKey = false;
var db = false;

module.exports = {
	allow: allow,
	cleanUp: cleanUp
};

function allow( o ) {
	// params:
	//   `key`
	//   `lockCollection` or `db` (with `connection`, `database` and `collection`)
	//   `ifAllowed`: function called if allowed to proceed
	//   `ifDisallowed`: function called if NOT allowed to proceed - receives
	//      null or error object as argument
	//   `maxParallel` (optional, defaults to DEFAULT_MAX_PARALLEL)
	//   `start` (optional, defaults to new Date() )

	var parallelLimit = o.maxParallel || DEFAULT_MAX_PARALLEL;
	var key = o.key;
	var start = o.start || new Date();
	var preexistCheck = false;

	if( didAllow ) {
		o.ifDisallowed();
		return;
	}

	if( o.lockCollection ) {
		setImmediate( curry( requestAllow, o.lockCollection ) );
	} else if( o.db && o.db.connection && o.db.database && o.db.collection ) {
		( new mongodb.MongoClient() ).connect( o.db.connection, clientConnected );
	} else {
		console.error( 'Missing o.lockCollection or o.db (with connection, database and collection) while trying to allow' );
	}

	function requestAllow( inLockCollection ) {
		var query = { key: key };
		var task;

		query[ 'tasks.' + ( parallelLimit - OFFSET_ONE ) ] = { $exists: 0 };
		task = { pid: process.pid, start: start };

		lockCollection = inLockCollection;
		lockCollection.findOneAndUpdate(
			query,
			{ $push: { tasks: task } },
			receivedAllow
		);

	}

	function receivedAllow( err, locking ) {

		if( locking.value ) {
			didAllow = true;
			allowedKey = key;
			o.ifAllowed();
			return;
		}

		if( preexistCheck ) {
			closeDb( function receivedDisallowDbClosed(){
				o.ifDisallowed();
			} );
		} else {
			preexistCheck = true;
			// make sure the key exists, then try once more
			lockCollection.findOne(
				{ key: key },
				{ _id: 1 },
				lockExists
			);
		}
	}

	function lockExists( err, preexisted ) {

		if( err ) {
			o.ifDisallowed(  { error: 'db', details: err } );
			return;
		}

		if( preexisted ) {
			closeDb( function preexistedDbClosed(){
				o.ifDisallowed();
			} );
			return;
		}

		// did not preexist - insert the lock, then try again
		lockCollection.findOneAndUpdate(
			{ key: key },
			{ $set: { key: key } },
			{ upsert: true },
			didUpsert
		);

	}

	function didUpsert( err ) {

		if( err ) {
			o.ifDisallowed(  { error: 'db', details: err } );
			return;
		}

		requestAllow( lockCollection );
	}

	function clientConnected( err, client ){

		if( err ) {
			o.ifDisallowed( { error: 'db', details: err } );
			return;
		}

		db = client.db( o.db.database );
		lockCollection = db.collection( o.db.collection );

		requestAllow( lockCollection );
	}
}


function cleanUp( cleanedUp ) {

	lockCollection.update(
		{ key: allowedKey },
		{ $pull: { tasks: { pid: process.pid } } },
		didUpdate
	);

	function didUpdate( err ) {
		closeDb( function cleanupClosedDb(){

			if( err ) {
				cleanedUp(  { error: 'db', details: err } );
				return;
			}
			didAllow = false;
			cleanedUp();
		} );
	}
}

function closeDb( callback ) {
	if( db ) {
		db.close( false, callback );
	} else {
		callback();
	}
}


function curry( f ) {
	var SKIP_FUNCTION = 1;

	// pre-define arguments, don't specify this
	var curriedArgs = Array.prototype.slice.call( arguments, SKIP_FUNCTION );

	return curriedArgs.length
		? function curried() {
			var allArgs = curriedArgs.slice( );
			var i;
			var n = arguments.length;
			for( i = 0; i < n; i += 1 ) {
				allArgs.push( arguments[ i ] );
			}
			f.apply( this, allArgs );
		} : f;
}
