# AllowParallel

Tries to add this task to a list of running tasks kept in the mongodb lockCollection under key 'key'. If there are already `maxParallel` tasks, runs `ifDisallowed`, otherwise `ifAllowed` (and does not add this task).

Example:

```js

allowParallel = require('allowparallel');

allowParallel.allow({
	// mongodb collection object to use - but see below
	lockCollection: lockCollection,
	key: 'myLock',
	ifAllowed: allowed,
	ifDisallowed: disallowed
});

function allowed(){
	// yay, we were allowed to do stuff
	...
	// now clean up
	allowParallel.cleanUp( shutdown );
}

function disallowed( err ) {
	if( err ) {
		console.error( 'Failed to request allow:', err );
	}
}

function shutdown( err ) {
	if( err ) {
		console.error( 'Failed to clean up allow:', err );
	}
}
```

## Configuring the collection

There are 2 options - either you pass in a MongoCollection in the `lockCollection` property of the configuration object, or a `db` property, which defines a mongo connection string as `connection`, the database name as `database`, and the collection name as `collection`.
